CreoPlayer is a free iOS application that you can use to preview your apps in real time on a real iOS device (iPhone and iPad). The latest version is available through the **[TestFlight](http://creoplayer.creolabs.com)** program. Applications can be transferred to the CreoPlayer using both WiFi or USB.





Start by opening the CreoPlayer iOS app:
![Creo](../images/creo/creoplayer-1.png)

Then press the paper plane icon in the Creo's toolbar:
![Creo](../images/creo/creoplayer-2.png)

A list of devices will appear, just press the Send button next to the device that will receive your app. Once pressed the app will be transferred to the CreoPlayer app:
![Creo](../images/creo/creoplayer-3.png)

Just select the app to execute it and **shake** your device to close the app. The shake to close gesture can be configured in CreoPlayer preferences panel:
![Creo](../images/creo/creoplayer-4.png)
