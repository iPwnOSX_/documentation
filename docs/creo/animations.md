User experience in modern mobile apps is boosted by animations, they are used everywhere to increase usability and to give you the touch and feeling of important tasks being performed.


Unfortunately creating efficient animations is not an easy task, you need to know a lot of implementation details, you need to spend time performing complex math operations and most of the time you don't have a real time feedback about what is happening. 
![Creo](../images/creo/animations-1.png)

**Creo** introduces a new way to create animations with an intuitive timeline interface like the one you can find in professional video maker tool. Just drop the object you want to animate into the animation panel and start experimenting with properties, settings and timing. Once done the result will be a native Core Animation object that will be executed at native speed on your device.