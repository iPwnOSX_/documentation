Let's try to answer the question: **what is a mobile application?** If you think a bit about mobile development or software development in general, you'll end up with a very simple answer. It is a **user interface**, **connected** with some **data**.


**Creo** offers an intuitive and efficient way to build mobile applications using these three main components:

1. A very intuitive **[Drag and Drop](user-interface-creation.md)** system that lets you build application's user interfaces in a very intuitive way.
2. A powerful **[DataSet](dataset.md)** subsystem that enables you to read and write data from a different variety of sources like network, databases, files, Bluetooth devices, sensors, camera and many others.
3. A very intuitive **[Events](objects-and-events.md)** based system where you can write the connection code between your user interface objects and your data objects.

![Creo](../images/creo/how-creo-works-1.png)

**Creo** is a next generation tool that lets you combine design and development in order to build better applications in a fraction of time compared to traditional tools.